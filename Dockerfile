FROM python:2-buster

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get update --fix-missing && \
    apt-get install -y wget ca-certificates vim git && \
    apt-get install -y doxygen

RUN pip install --upgrade pip && \
    pip install coverage && \
    pip install coverage-badge && \
    pip install pylint && \
    pip install anybadge && \
    pip install pyinstaller

CMD ["/bin/bash"]
