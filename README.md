# build-python

## image

Python docker image.

The project builds the image and push it to docker-hub. Two images are pushed: 
- tagged version: thanks to DOCKER_IMAGE_VERSION parameter
- latest version

## docker-hub
This image is available on docker-hub:
https://hub.docker.com/r/balacsoft/build-python

To pull the image:
docker pull balacsoft/build-python

## content

- python2 and python3 : python binaries
- pylint: static and rules analysis
- anybadge: allow creation of badge for gitlab
- pyinstaller: allow build of executable
- coverage: for unitary tests and test coverage
